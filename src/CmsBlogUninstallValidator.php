<?php

namespace Drupal\cms_blog;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleUninstallValidatorInterface;

/**
 * Prevents CMS Blog module from being uninstalled if any blog entries exist.
 */
class CmsBlogUninstallValidator implements ModuleUninstallValidatorInterface {

  /**
   * An instance of the entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a new CmsBlogUninstallValidator.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager) {
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public function validate($module) {
    $reasons = [];

    if ($module == 'cms_blog') {

      if ($this->hasBlogNodes()) {
        $reasons[] = t('To uninstall CMS Blog module, first delete all <em>Blog</em> content');
      }

      if ($this->hasTerms('cms_blog_category')) {
        $reasons[] = t('To uninstall CMS Blog module, first delete all terms from Blog category vocabulary.');
      }

      if ($this->hasTerms('cms_blog_tags')) {
        $reasons[] = t('To uninstall CMS Blog module, first delete all terms from Blog tags vocabulary.');
      }

    }

    return $reasons;
  }

  /**
   * Determines if there is any CMS Blog nodes or not.
   *
   * @return bool
   *   TRUE if there are blog nodes, FALSE otherwise.
   */
  protected function hasBlogNodes() {
    $query = $this->entityTypeManager->getStorage('node')->getQuery();
    $nodes = $query->condition('type', 'cms_blog')
      ->accessCheck(FALSE)
      ->range(0, 1)
      ->execute();
    return !empty($nodes);
  }

  /**
   * Determines if there are any taxonomy terms for a specified vocabulary.
   *
   * @param int $vid
   *   The ID of vocabulary to check for terms.
   *
   * @return bool
   *   TRUE if there are terms for this vocabulary, FALSE otherwise.
   */
  protected function hasTerms($vid) {
    $query = $this->entityTypeManager->getStorage('taxonomy_term')->getQuery();
    $terms = $query->condition('vid', $vid)
      ->accessCheck(FALSE)
      ->range(0, 1)
      ->execute();
    return !empty($terms);
  }

}
